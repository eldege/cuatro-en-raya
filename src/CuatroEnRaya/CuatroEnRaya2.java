package CuatroEnRaya;

import javax.print.DocFlavor.URL;
import javax.swing.*;
import java.awt.*;
import java.lang.*;
import java.lang.Object;
import java.awt.event.*;
import java.util.Random;

public class CuatroEnRaya2 {
	


	static class Raya extends JFrame implements ActionListener
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		int turno=0;
		boolean fin;
		boolean dosJugadores;
		boolean pulsado;

		JButton boton[][]=new JButton[7][7];

		JMenuBar Barra=new JMenuBar();
		JMenu Archivo=new JMenu("Archivo");
		JMenu Opciones=new JMenu("Modos");
		JMenuItem Nuevo=new JMenuItem("Nuevo");
		JMenuItem Salir=new JMenuItem("Salir");
		JMenuItem M1Jugador=new JMenuItem("1 Jugador");
		JMenuItem M2Jugador=new JMenuItem("2 Jugadores");
		JLabel Nombre=new JLabel("***Cuatro en raya - Daniel García***",JLabel.CENTER);
		

		ImageIcon foto1;
		ImageIcon foto2;
		
		Raya()
		{

			
			
		
			foto1 = new ImageIcon("img/circulo.png");
			foto2 = new ImageIcon("img/x.png");
			
			

			Nuevo.addActionListener(this);
			Archivo.add(Nuevo);
			Archivo.addSeparator();
			Salir.addActionListener(this);
			Archivo.add(Salir);
			M1Jugador.addActionListener(this);
			Opciones.add(M1Jugador);
			M2Jugador.addActionListener(this);
			Opciones.add(M2Jugador);
			Barra.add(Archivo);
			Barra.add(Opciones);
			setJMenuBar(Barra);
			
			

			JPanel Principal=new JPanel();
			Principal.setLayout(new GridLayout(7,7));


			for(int i=0;i<7;i++)
			{
				for(int j=0;j<7;j++)
				{
					
					boton[i][j]=new JButton();
					boton[i][j].addActionListener(this);
					Color fondo = new Color(188,188,188);
					boton[i][j].setBackground(fondo);
					Principal.add(boton[i][j]);
				}
				Nombre.setForeground(Color.black);
				add(Nombre,"North");
				add(Principal,"Center");
				
			}
			

			
			addWindowListener(new WindowAdapter()
			{
				public void windowClosing(WindowEvent we)
				{
					System.exit(0);
				}
			});
			

			setLocation(170,25);
			setSize (600,600);
			setResizable(false);
			setTitle("CUATRO EN RAYA");
			setVisible(true);
			
		}
		void ganar(int x,int y)
		{

			 
			fin=false;

				int ganar1=0;
			    int ganar2=0;
			    for(int j=0;j<7;j++)
		     	{
				    if(boton[y][j].getIcon()!=null)
				    {
					     if(boton[y][j].getIcon().equals(foto1))
					     {
						    ganar1++;
					     }
					     else ganar1=0;
					     if(ganar1==4)
					     {
					     	 
						      JOptionPane.showMessageDialog(this,"Gana Jugador 1","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto1);
						      VolverEmpezar();
						      fin=true;
					     }
					     if(fin!=true)
					     {
					     	if(boton[y][j].getIcon().equals(foto2))
					        {
						           ganar2++;
					        }
					        else ganar2=0;
					        if(ganar2==4)
					        {
					          JOptionPane.showMessageDialog(this,"Gana Jugador 2","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto2);	
					          VolverEmpezar();
					        }
					     }
			    	}
			    	 else
			         {
			    	   ganar1=0;
			    	   ganar2=0;
			         }
			   }

				 ganar1=0;
			     ganar2=0;
			    for(int i=0;i<7;i++)
		     	{
				    if(boton[i][x].getIcon()!=null)
				    {
					     if(boton[i][x].getIcon().equals(foto1))
					     {
						    ganar1++;
					     }
					     else ganar1=0;
					     if(ganar1==4)
					     {
						      JOptionPane.showMessageDialog(this,"Gana Jugador 1","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto1);
						      VolverEmpezar();
						      fin=true;
					     }
					     if(fin!=true)
					     {
					     	if(boton[i][x].getIcon().equals(foto2))
					     	{
					     		ganar2++;
					     	}
					        else ganar2=0;
					        if(ganar2==4)
					        {
					          JOptionPane.showMessageDialog(this,"Gana Jugador 2","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto2);
					          VolverEmpezar();	
					        }
					     }
			    	}
			    }

			 ganar1=0;
			 ganar2=0;
				int a=y;
				int b=x;
			    	while(b>0 && a>0)
			    	{
			    		a--;
			    		b--;   		
			    	}
			    	while(b<7 && a<7)
			    	{	
			    	 if(boton[a][b].getIcon()!=null)
			    	 {		    		
			    		if(boton[a][b].getIcon().equals(foto1))
					     {
						    ganar1++;
					     }
					     else ganar1=0;
					     if(ganar1==4)
					     {
						      JOptionPane.showMessageDialog(this,"Gana Jugador 1","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto1);
						      VolverEmpezar();
						      fin=true;	    
					     }
					     if(fin!=true)
					     {
					     	if(boton[a][b].getIcon().equals(foto2))
					     	{
					     		ganar2++;
					     	}
					     
					        else ganar2=0;
					        if(ganar2==4)
					        {
					          JOptionPane.showMessageDialog(this,"Gana Jugador 2","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto2);
					          VolverEmpezar();	
					        }   
					     }
					 }
					 else 
					 {
					 	ganar1=0;
					 	ganar2=0;
					 } 
			    	    a++;
			    		b++;  
			         } 
 
		        ganar1=0;
			    ganar2=0;
				 a=y;
				 b=x;

			    	while(b<6 && a>0)
			    	{
			    		a--;
			    		b++;
			    		
			    	}
			    	while(b>-1 && a<7 )
			    	{
			    	 if(boton[a][b].getIcon()!=null)
			    	 {		
			    		if(boton[a][b].getIcon().equals(foto1))
					     {
						    ganar1++;
					     }
					     else ganar1=0;
					     if(ganar1==4)
					     {
						      JOptionPane.showMessageDialog(this,"Gana Jugador 1","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto1);
						      VolverEmpezar();
						      fin=true;	    
					     }
					     if(fin!=true)
					     {
					     	if(boton[a][b].getIcon().equals(foto2))
					     	{
					     		ganar2++;
					       	}
					        else ganar2=0;
					        if(ganar2==4)
					        {
					          JOptionPane.showMessageDialog(this,"Gana Jugador 2","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE,foto2);
					          VolverEmpezar();	
					        }
					     }
					 } 
					 else 
					 {
					 	ganar1=0;
					 	ganar2=0;
					 } 
			    	    
			    	 a++;
			         b--;
			         } 
			}

		void VolverEmpezar()
			{
				for(int i=0;i<7;i++)
				{
					for(int j=0;j<7;j++)
					{
						boton[i][j].setIcon(null);
					}
				}
				turno=0;
			}
			
		 public void actionPerformed(ActionEvent ae)
			 {
			   if(ae.getSource()==M1Jugador)
			   {
			   	dosJugadores=false;
			   	VolverEmpezar();
			   }	
			   if(ae.getSource()==M2Jugador)
			   {
			   	dosJugadores=true;
			   	VolverEmpezar();
			   }
	           if(ae.getSource()==Salir)
			    {	
			      dispose();
			    }
			   if(ae.getSource()==Nuevo)
			    {
			      VolverEmpezar();
			    }
			 	int x=0;
			 	int y=0;
			 	for(int i=0;i<7;i++)
			 	{
			 		for(int j=0;j<7;j++)
			 		{
			 			    if (ae.getSource()==boton[i][j])
			 			    {	

			 				int k=7;
			 				do
			 				{
			 					k--;
			 				}
			 				while(boton[k][j].getIcon()!=null & k!=0);

								if(boton[k][j].getIcon()==null)
			 				 	{	
			 				 	  	if(dosJugadores)
							         {
			 				  			if(turno %2==0)boton[k][j].setIcon(foto1);
			 				  			else boton[k][j].setIcon(foto2);
			 			      			turno++;
			 			      			x=j;
			 			      			y=k;
								     }
								    else
			 			        	{
			 			        	 boton[k][j].setIcon(foto1);
			 			        	 turno++;
			 			        	 pulsado=true;
			 			      		 x=j;
			 			      		 y=k;
			 			        	} 
			 			        } 
			 			}
			 		}
			 	}
			 	ganar(x,y);
			 	if(pulsado)
			 		{
			 	      if(!dosJugadores) juegaMaquina(x,y); 
			 	  	} 


			    if(turno==49)  
			    {
			    	 JOptionPane.showMessageDialog(this,"Empate","Cuatro En Raya",JOptionPane.INFORMATION_MESSAGE);
					 VolverEmpezar();
			    }
			    fin=false;
			}
		void juegaMaquina(int x,int y)
		{
			boolean Cubrir_izquierda=false;
			int ganarVert=0;	
			int ganarHorz=0;
			int posicion=0;
	        posicion=GenerarAleatorio(x);
	               

		ganarHorz=0;
		
		  for(int i=0;i<7;i++)
		     	{
		     		for(int j=0;j<7;j++)
		     		{
		     			
		     		if(boton[i][j].getIcon()!=null)
				    	{
					     
					     if(boton[i][j].getIcon().equals(foto2))
					     		      {
									    ganarHorz++;
					     			  }
					     			else ganarHorz=0;
					     			if(ganarHorz==3)
					     		      {
				 		 				posicion=j;
				 		 				if(posicion!=6) 
				 		 				{
				 		 				  if(boton[y][j+1].getIcon()==null)	posicion++;
				 		 				  else if(j>=3 && boton[y][j-3].getIcon()==null)posicion=posicion-3;
				 		 				  System.out.println("Por la derecha");
				 		 				}
				 		 			 }
				 		 }			 
		     		}
				ganarHorz=0;
			   }
	        	

				ganarHorz=0;
	  			for(int j=6;j>=0;j--)
		    		 	{
						    if(boton[y][j].getIcon()!=null)
				    			{
					    	 		if(boton[y][j].getIcon().equals(foto1))
					     		      {
									    ganarHorz++;
					     			  }
					     			else ganarHorz=0;
					     			if(ganarHorz==3 && j!=0)
					     		      {
					     		      	posicion=j;
				 		 			
				 		 				  if(boton[y][j-1].getIcon()==null)
				 		 				  {
				 		 				  	posicion--;
				 		 				  	Cubrir_izquierda=true;
				 		 				  }	

				 	     	       	  }
				 			}
				 		} 
				 		

				ganarHorz=0;
	  		if(!Cubrir_izquierda)
	  		{
	  			for(int j=0;j<7;j++)
		    		 	{
						    if(boton[y][j].getIcon()!=null)
				    			{
					    	 		if(boton[y][j].getIcon().equals(foto1))
					     		      {
									    ganarHorz++;
					     			  }
					     			else ganarHorz=0;
					     			if(ganarHorz==3)
					     		      {
				 		 				posicion=j;
				 		 				if(posicion!=6) 
				 		 				{
				 		 				  if(boton[y][j+1].getIcon()==null)	posicion++;

				 		 				}
				 	     			}
				 				} 
				 			}
	  		}	
	  			      

			    for(int i=0;i<7;i++)
		     	{
				    if(boton[i][x].getIcon()!=null)
				    {
					     if(boton[i][x].getIcon().equals(foto1))
					     {
						    ganarVert++;
					     }
					     else ganarVert=0;
					     if(ganarVert==3)
					     {
					     	  posicion=x;
					     }
			    	}
			    }

				    
			 ganarVert=0; 
			 for(int i=0;i<7;i++)
		     	{
		     		for(int j=0;j<7;j++)
		     		{
		     		if(boton[j][i].getIcon()!=null)
				    	{
									     
					     	if(boton[j][i].getIcon().equals(foto2))
					     	{
						    	ganarVert++;
					     	}
					     	else ganarVert=0;
					     	if(ganarVert==3 && j!=0)
					     	{
					     	  posicion=i;
					     	}
			    		}
			   		}
			   ganarVert=0;
			   }
				 		
				 	if(boton[0][posicion].getIcon()!=null)
			 	     { 	
			 	  	posicion=GenerarAleatorio(posicion);
			 	     }	
			 		 	int k=7;

			 		 	do
			 		 	 {
			 		 	 	k--;
			 		 	 }
			 		 	 while(boton[k][posicion].getIcon()!=null & k!=0);

			 		 	    boton[k][posicion].setIcon(foto2);
			ganar(posicion,k);
			pulsado=false;  
			Cubrir_izquierda=false;      
		}	 
		int GenerarAleatorio(int posicion)
		{

			double aleatorio=0;
			do
		 	{
				aleatorio=Math.random()*7;
		 		posicion=(int)aleatorio;
		 	}
		 	while(boton[0][posicion].getIcon()!=null);
		 	return posicion;
		}

		public static void main (String [] args)
		{
			new Raya();
		}
		
	}
	
}
